import pytest
from vndbpy import VndbAPi, VndbConnection


@pytest.fixture(scope='session')
def conn(request):
    return VndbConnection()


@pytest.fixture(scope='session')
def api(request):
    return VndbAPi()
