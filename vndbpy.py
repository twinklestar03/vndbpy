import socket
import ssl
from exceptions import *

try:
    import ujson as json
except ImportError:
    import json


class VndbAPi:
    def __init__(self, username, passwd, conn=None):            
        self.conn = conn or VndbConnection(username=username, password=passwd)

    def get_character(self, filters: dict, tags: tuple):
        tags = self._parse_tags(tags)
        filters = self._parse_filter(filters)
        cmd = f'get character {tags} {filters}'
        data = self.conn.send_command(cmd)
        
        pass

    def get_vn(self, filters: dict, tags: tuple):
        tags = self._parse_tags(tags)
        filters = self._parse_filter(filters)
        pass

    def get_release(self, filters: dict, tags: tuple):
        tags = self._parse_tags(tags)
        filters = self._parse_filter(filters)
        pass

    @staticmethod
    def _parse_tags(tags: tuple):
        tag_str = ','.join(f'{s}' for s in tags)
        return tag_str

    @staticmethod
    def _parse_filter(filters: dict):
        i = 0
        filter_str = '('
        for k, v in filters.items():
            filter_str += f'{k} = {v}'
            filter_str += ','if i < len(filters)-1 else ''
            i += 1
        filter_str += ')'
        return filter_str

class VndbConnection:
    def __init__(self, username=None, password=None):
        """
        Just a lowly connection handler for VNDB
        """
        self.clientvars = {'protocol': 1, 'clientver': 0.1, 'client': 'VndbPy'}
        self.loggedin = False
        self.data_buffer = bytes(1024)
        self.sslcontext = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
        self.sslcontext.verify_mode = ssl.CERT_REQUIRED
        self.sslcontext.check_hostname = True
        self.sslcontext.load_default_certs()
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sslwrap = self.sslcontext.wrap_socket(self.socket, server_hostname='api.vndb.org')
        self.sslwrap.connect(('api.vndb.org', 19535))
        self.login(username, password)

    def close(self):
        self.sslwrap.close()

    def login(self, username, password):
        finvars = self.clientvars
        if username and password:
            finvars['username'] = username
            finvars['password'] = password
            self.loggedin = True
            ret = self.send_command('login', json.dumps(finvars))
            if not isinstance(ret, str):  # should just be 'Ok'
                if self.loggedin:
                    self.loggedin = False
                    raise LoginError(ret['msg'])
                else:
                    raise LoginError(ret['msg'])

    def send_command(self, command, args=None, retries=3):
        if args:
            if isinstance(args, str):
                final_command = command + ' ' + args + '\x04'
            else:
                # We just let json propogate the error here if it can't parse the arguments
                final_command = command + ' ' + json.dumps(args) + '\x04'
        else:
            final_command = command + '\x04'

        try:
            self.sslwrap.sendall(final_command.encode('utf-8'))
        except OSError:
            if retries > 0:
                self.send_command(final_command, retries=retries-1)

        return self._recv_data()

    def _recv_data(self, temp='', retries=3):
        try:
            while True:
                self.data_buffer = self.sslwrap.recv(1024)
                if '\x04' in self.data_buffer.decode('utf-8', 'ignore'):
                    temp += self.data_buffer.decode('utf-8', 'ignore')
                    break
                else:
                    temp += self.data_buffer.decode('utf-8', 'ignore')
                    self.data_buffer = bytes(1024)
            temp = temp.replace('\x04', '')

        except OSError:
            if retries > 0:
                self._recv_data(temp=temp, retries=retries-1)
        
        if 'ok' in temp:  # Because login
            return temp

        else:
            return json.loads(temp.split(' ', 1)[1])
