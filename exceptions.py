class VndbPyErrpr(Exception):
    pass


class LoginError(VndbPyErrpr):
    pass


class ArgumentsError(VndbPyErrpr):
    pass
